# https://github.com/grpc/grpc/blob/v1.62.0/examples/ruby/greeter_client.rb

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'grpc'
  gem 'debug'
end

require 'benchmark'

this_dir = File.expand_path(File.dirname(__FILE__))
lib_dir = File.join(this_dir, 'lib')
$LOAD_PATH.unshift(lib_dir) unless $LOAD_PATH.include?(lib_dir)

require 'flagd/evaluation/v1/evaluation_services_pb'

def ouput_cost_of_one_call(operation_name)
  total_calls = 5000
  result = Benchmark.measure { total_calls.times { yield } }
  seconds_per_call = result.total / total_calls
  milliseconds_per_call = seconds_per_call * 1000
  puts "#{operation_name} costs #{milliseconds_per_call.round(3)} ms per call (#{result.total.round(3)}s for #{total_calls} calls)"
end

def main
  user = ARGV.size > 0 ?  ARGV[0] : 'world'
  hostname = ARGV.size > 1 ?  ARGV[1] : 'localhost:8013'
  stub = OpenFeature::Flagd::Provider::Grpc::Evaluation::Service::Stub.new(hostname, :this_channel_is_insecure)
  begin
    ouput_cost_of_one_call('grpc!') { stub.resolve_boolean(OpenFeature::Flagd::Provider::Grpc::Evaluation::ResolveBooleanRequest.new(flag_key: 'show-welcome-banner')) }
  rescue GRPC::BadStatus => e
    abort "ERROR: #{e.message}"
  end
end

main

