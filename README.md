Start flagd (https://flagd.dev/quick-start/):

```bash
docker run \
  --rm -it \
  --name flagd \
  -p 8013:8013 \
  -v $(pwd):/etc/flagd \
  ghcr.io/open-feature/flagd:latest start \
  --uri file:./etc/flagd/demo.flagd.json
```

Then run the ruby script: `ruby test.rb`

Example results:

```bash
simple_boolean_flag costs 0.543 ms per call (2.714s for 5000 calls)
simple_boolean_flag_with_targeting costs 0.532 ms per call (2.661s for 5000 calls)
multivariant costs 0.551 ms per call (2.753s for 5000 calls)
```
