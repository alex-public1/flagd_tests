#!/usr/bin/env ruby

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'openfeature-sdk', github: 'open-feature/ruby-sdk'
  gem 'httpx'
  gem 'debug'
end

require 'benchmark'
# require 'json' # For JSON.dump

# module OpenFeature
#   module SDK
#     module Provider
#       # Defines the default provider that is set if no provider is specified.
#       #
#       # To use <tt>NoOpProvider</tt>, it can be set during the configuration of the SDK
#       #
#       #   OpenFeature::SDK.configure do |config|
#       #     config.provider = NoOpProvider.new
#       #   end
#       #
#       # Within the <tt>NoOpProvider</tt>, the following methods exist
#       #
#       # * <tt>fetch_boolean_value</tt> - Retrieve feature flag boolean value
#       #
#       # * <tt>fetch_string_value</tt> - Retrieve feature flag string value
#       #
#       # * <tt>fetch_number_value</tt> - Retrieve feature flag number value
#       #
#       # * <tt>fetch_object_value</tt> - Retrieve feature flag object value
#       #
#       class FlagdProvider
#         REASON_NO_OP = "No-op"
#         NAME = "Flagd Provider"

#         attr_reader :metadata

#         def initialize
#           @metadata = Metadata.new(name: NAME).freeze
#         end

#         def fetch_boolean_value(flag_key:, default_value:, evaluation_context: nil)
#           no_op(default_value)
#         end

#         def fetch_string_value(flag_key:, default_value:, evaluation_context: nil)
#           no_op(default_value)
#         end

#         def fetch_number_value(flag_key:, default_value:, evaluation_context: nil)
#           no_op(default_value)
#         end

#         def fetch_object_value(flag_key:, default_value:, evaluation_context: nil)
#           no_op(default_value)
#         end

#         private

#         def no_op(default_value)
#           ResolutionDetails.new(value: default_value, reason: REASON_NO_OP)
#         end
#       end
#     end
#   end
# end

# # API Initialization and configuration

# debugger

# OpenFeature::SDK.configure do |config|
#     # your provider of choice
#     config.provider = OpenFeature::SDK::Provider::FlagdProvider.new
# end

# # Create a client
# client = OpenFeature::SDK.build_client(name: "my-app")

# # fetching boolean value feature flag
# bool_value = client.fetch_boolean_value(flag_key: 'boolean_flag', default_value: false)

# # fetching string value feature flag
# string_value = client.fetch_string_value(flag_key: 'string_flag', default_value: false)

# # fetching number value feature flag
# float_value = client.fetch_number_value(flag_key: 'number_value', default_value: 1.0)
# integer_value = client.fetch_number_value(flag_key: 'number_value', default_value: 1)

# # get an object value
# object = client.fetch_object_value(flag_key: 'object_value', default_value: JSON.dump({ name: 'object'}))

module FeatureFlagResolver
  module_function

  FLAGD_URL = 'http://localhost:8013/flagd.evaluation.v1.Service/%{url_suffix}'

  def resolve_boolean(key, context: {})
    resolve_feature_flag(:boolean, key, context)
  end

  def resolve_string(key, context: {})
    resolve_feature_flag(:string, key, context)
  end

  def resolve_feature_flag(feature_flag_type, flag_key, context)
    url_suffix = case feature_flag_type
    in :boolean
      'ResolveBoolean'
    in :string
      'ResolveString'
    end

    HTTPX.post(FLAGD_URL % { url_suffix: url_suffix }, json: { flagKey: flag_key, context: context })
  end
end

def ouput_cost_of_one_call(operation_name)
  total_calls = 5000
  result = Benchmark.measure { total_calls.times { yield } }
  seconds_per_call = result.total / total_calls
  milliseconds_per_call = seconds_per_call * 1000
  puts "#{operation_name} costs #{milliseconds_per_call.round(3)} ms per call (#{result.total.round(3)}s for #{total_calls} calls)"
end

ouput_cost_of_one_call('simple_boolean_flag') { FeatureFlagResolver.resolve_boolean('show-welcome-banner') }
ouput_cost_of_one_call('simple_boolean_flag_with_targeting') { FeatureFlagResolver.resolve_boolean('show-welcome-banner', context: { 'new-joiner' => true }) }
ouput_cost_of_one_call('multivariant') { FeatureFlagResolver.resolve_string('background-color', context: { 'targetingKey' => "sessionId-#{rand(1..100)}" }) }

# ApplicationPreference
# ApplicationPreference.monitor_index_usage_www_enabled?
# ouput_cost_of_one_call('application_preference') { ApplicationPreference.monitor_index_usage_www_enabled? }
# application_preference costs 0.013 ms per call (0.067s for 5000 calls)
